#define _DEFAULT_SOURCE
#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"


#include <stdarg.h>
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>



void test_5(void* mem_heap) {
    printf("\n---Пятый тест. Регион расширяется где-то в другом месте---\n");
    debug_heap(stdout,mem_heap);
    _malloc(200);
    void* map = mmap((void*)0x4044000, 1024, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0);
    printf("%p\n",map);
    _malloc(80000);
    debug_heap(stdout,mem_heap);
}

void test_1(void* mem_heap){
    printf("---Первый тест. Успешно выделяем и освобождаем память---\n");
    debug_heap(stdout,mem_heap);
    void* malloc1 = _malloc(100);
    debug_heap(stdout, mem_heap);
    _free(malloc1);
    debug_heap(stdout, mem_heap);
}

void test_2(void* mem_heap){
    printf("\n---Второй тест. Выделяем несколько блоков. Освобождаем 1---\n");
    debug_heap(stdout, mem_heap);
    void* malloc1 = _malloc(20);
    void* malloc2 = _malloc(1000);
    void* malloc3 = _malloc(10);
    debug_heap(stdout, mem_heap);
    _free(malloc2);
    debug_heap(stdout, mem_heap);
    _free(malloc1);
    _free(malloc3);
}

void test_3(void* mem_heap){
    printf("\n---Третий тест. Выделяем несколько, освобождаем 2---\n");
    debug_heap(stdout,mem_heap);
    void* malloc1 = _malloc(20);
    void* malloc2 = _malloc(1000);
    void* malloc3 = _malloc(10);
    debug_heap(stdout,mem_heap);
    _free(malloc3);
    _free(malloc2);
    debug_heap(stdout,mem_heap);
    _free(malloc1);
}

void test_4(void* mem_heap){
    printf("\n---Четвертый тест. Память закончилась, расширяем старый регион---\n");
    debug_heap(stdout,mem_heap);
    void* malloc = _malloc(8176);
    debug_heap(stdout,mem_heap);
    _free(malloc);
    debug_heap(stdout, mem_heap);
}