#include <stdio.h>
#include "mem.h"

#include "mem_internals.h"
#include "tests.h"



int main(){

    /* Надеюсь я правильно уловил смысл дебаг функций, а также то, как следует демонстрировать работоспособность программы */
    void* mem_heap = heap_init(2*REGION_MIN_SIZE);

    test_1(mem_heap);

    test_2(mem_heap);

    test_3(mem_heap);

    test_4(mem_heap);

    test_5(mem_heap);

}
